Welcome to my Connect 4 Game!

This implementation was created in Cloud 9 IDE using their basic Node.js
workspace.

Start the server from a terminal with:
    $ node server.js
    
Once the server is running, open the project in a browser window with:

https://mjr-connect-4-mjresman.c9users.io/
      var currentColor = "red";
      var board = [[0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0]];
                   
      var row = 0;
      var countToWin = 4;
      var winner;
      
      function dropToBottom(row, column) {
        for (var y = 5; y > row; y--){
          if (board[y][column] === 0) {
              return y;
          }
        }
        return row;
      }
      
      function updateBoard(column) {
        var colNumber = column - 1;
        var rowNumber = dropToBottom( 0, colNumber);
        board[rowNumber][colNumber] = currentColor;
        var cellId = "r" + rowNumber + "c" + colNumber;
        var newImage = (currentColor === "red" ? "<img src=\'img/image2.jpg\' />" : "<img src=\'img/image3.jpg\' />");
        document.getElementById(cellId).innerHTML = newImage;
        if ( checkVertical() || checkHorizontal() || checkDiagonal() ) {
          var winner = (currentColor === "red" ? "Red" : "Yellow");
          if (!alert(winner + " Wins!")){window.location.reload()}
        }
        else {
          currentColor = (currentColor === "yellow" ? "red":"yellow");
          alert("Next color to play: " + currentColor);
        }
      }
      
      function checkHorizontal() {
        var lastValue = 0;
        var currentValue = null;
        var consecutive = 0;
        for (var row = 5; row >= 0; row--){
          for (var col = 0; col <= 6; col++){
            currentValue = board[row][col];
            if (currentValue === lastValue && currentValue !== 0) {
              consecutive++;
            } else {
              consecutive = 0;
            }
            if (consecutive === countToWin - 1){
                return true;
            }
            lastValue = currentValue;
          }
          consecutive = 0;
          lastValue = 0;
        }
        return false;
      }
      
      function checkVertical() {
        var lastValue = 0;
        var currentValue = 0;
        var consecutive = 0;
        for (var col = 0; col <= 6; col++){
          for (var row = 5; row >= 0; row--){
            currentValue = board[row][col];
            if (currentValue === lastValue && currentValue !== 0) {
              consecutive++;
            } else {
              consecutive = 0;
            }
            if (consecutive === countToWin - 1){
                return true;
            }
            lastValue = currentValue;
          }
          consecutive = 0;
          lastValue = 0;
        }
        return false;
      }
      
      function checkDiagonal() {
        var rowTemp = 0;
        var col, colTemp = 0;
        var lastValue = 0;
        var currentValue = 0;
        var consecutive = 0;
        
        // check right diagonal from bottom left (5,0)
        row = 5;
        for (col = 0; col <= 7; col++){
            rowTemp = 5;
            colTemp = col;
            while (colTemp <= 6 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp++;
            }
            consecutive = 0;
            lastValue = 0;
        }
        // check left diagonal from bottom right (5,6)
        row = 5;
        for (col = 6; col >= 0; col--){
            rowTemp = 5;
            colTemp = col;
            while (colTemp >= 0 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp--;
            }
            consecutive = 0;
            lastValue = 0;
        }
        
        // check right diagonal from left side (row 4 only)
        row = 4;
        for (col = 0; col <= 5; col++){
            rowTemp = 4;
            colTemp = col;
            while (colTemp <= 6 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp++;
            }
            consecutive = 0;
            lastValue = 0;
        }
        
        // check right diagonal from left side (row 3 only)
        row = 3;
        for (col = 0; col <= 4; col++){
            rowTemp = 3;
            colTemp = col;
            while (colTemp <= 4 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp++;
            }
            consecutive = 0;
            lastValue = 0;
        }
        
        // check left diagonal from right side (row 4 only)
        row = 4;
        for (col = 6; col >= 3; col--){
            rowTemp = 4;
            colTemp = col;
            while (colTemp >= 0 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp--;
            }
            consecutive = 0;
            lastValue = 0;
        }
        
        // check left diagonal from right side (row 3 only)
        row = 3;
        for (col = 6; col >= 3; col--){
            rowTemp = 3;
            colTemp = col;
            while (colTemp >= 0 && rowTemp >= 0) {
                currentValue = board[rowTemp][colTemp];
                if (currentValue != 0 && currentValue === lastValue) {
                    consecutive++;
                } else {
                    consecutive = 0;
                }
                if (consecutive == countToWin - 1) {
                    return true;
                }
                lastValue = currentValue;
                rowTemp--;
                colTemp--;
            }
            consecutive = 0;
            lastValue = 0;
        }
      }
